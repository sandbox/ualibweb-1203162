<?php

function ticker_views_default_views(){

$view = new view; 
$view->name = 'News_Ticker'; 
$view->description = ''; 
$view->tag = ''; 
$view->view_php = ''; 
$view->base_table = 'node'; 
$view->is_cacheable = FALSE; 
$view->api_version = 2; 
$view->disabled = FALSE;
 /* Edit this to true to make a default view disabled initially */ 
$handler = $view->new_display('default', 'Defaults', 'default'); 
$handler->override_option('fields', array( 
  'field_news_ticker_text_value' => array( 
    'label' => '', 
    'alter' => array( 
      'alter_text' => 0, 
      'text' => '', 
      'make_link' => 0, 
      'path' => '',
      'link_class' => '', 
      'alt' => '', 
      'prefix' => '',
      'suffix' => '',
      'target' => '',
      'help' => '',
      'trim' => 0,
      'max_length' => '',
      'word_boundary' => 1,
      'ellipsis' => 1,
      'html' => 0,
      'strip_tags' => 0,
    ),
    'empty' => '',
    'hide_empty' => 0,
    'empty_zero' => 0,
    'link_to_node' => 1,
    'label_type' => 'none',
    'format' => 'default',
    'multiple' => array(
      'group' => TRUE,
      'multiple_number' => '',
      'multiple_from' => '',
      'multiple_reversed' => FALSE,
    ),
    'exclude' => 0,
    'id' => 'field_news_ticker_text_value',
    'table' => 'node_data_field_news_ticker_text',
    'field' => 'field_news_ticker_text_value',
    'override' => array(
      'button' => 'Override',
    ),
    'relationship' => 'none',
  ),
  'field_news_start_date_value' => array(
    'label' => 'News Start Date',
    'alter' => array(
      'alter_text' => 0,
      'text' => '',
      'make_link' => 0,
      'path' => '',
      'link_class' => '',
      'alt' => '',
      'prefix' => '',
      'suffix' => '',
      'target' => '',
      'help' => '',
      'trim' => 0,
      'max_length' => '',
      'word_boundary' => 1,
      'ellipsis' => 1,
      'html' => 0,
      'strip_tags' => 0,
    ),
    'empty' => '',
    'hide_empty' => 0,
    'empty_zero' => 0,
    'link_to_node' => 0,
    'label_type' => 'widget',
    'format' => 'default',
    'multiple' => array(
      'multiple_number' => '',
      'multiple_from' => '',
      'multiple_to' => '',
      'group' => TRUE,
    ),
    'repeat' => array(
      'show_repeat_rule' => '',
    ),
    'fromto' => array(
      'fromto' => 'both',
    ),
    'exclude' => 0,
    'id' => 'field_news_start_date_value',
    'table' => 'node_data_field_news_start_date',
    'field' => 'field_news_start_date_value',
    'relationship' => 'none',
  ),
  'field_news_end_date_value' => array(
    'label' => 'News End Date',
    'alter' => array(
      'alter_text' => 0,
      'text' => '',
      'make_link' => 0,
      'path' => '',
      'link_class' => '',
      'alt' => '',
      'prefix' => '',
      'suffix' => '',
      'target' => '',
      'help' => '',
      'trim' => 0,
      'max_length' => '',
      'word_boundary' => 1,
      'ellipsis' => 1,
      'html' => 0,
      'strip_tags' => 0,
    ),
    'empty' => '',
    'hide_empty' => 0,
    'empty_zero' => 0,
    'link_to_node' => 0,
    'label_type' => 'widget',
    'format' => 'default',
    'multiple' => array(
      'multiple_number' => '',
      'multiple_from' => '',
      'multiple_to' => '',
      'group' => TRUE,
    ),
    'repeat' => array(
      'show_repeat_rule' => '',
    ),
    'fromto' => array(
      'fromto' => 'both',
    ),
    'exclude' => 0,
    'id' => 'field_news_end_date_value',
    'table' => 'node_data_field_news_end_date',
    'field' => 'field_news_end_date_value',
    'relationship' => 'none',
  ),
));
 
$handler->override_option('sorts', array( 
  'field_news_sort_priority_value' => array(
    'order' => 'DESC',
    'delta' => -1,
    'id' => 'field_news_sort_priority_value',
    'table' => 'node_data_field_news_sort_priority',
    'field' => 'field_news_sort_priority_value',
    'relationship' => 'none',
  ),
  'created' => array(
    'order' => 'DESC',
    'granularity' => 'day',
    'id' => 'created',
    'table' => 'node',
    'field' => 'created',
    'override' => array(
      'button' => 'Override',
    ),
    'relationship' => 'none',
  ),
));
 
$handler->override_option('filters', array( 
  'field_news_ticker_text_value' => array(
    'operator' => 'not empty',
    'value' => '',
    'group' => '0',
    'exposed' => FALSE,
    'expose' => array(
      'operator' => FALSE,
      'label' => '',
    ),
    'case' => 0,
    'id' => 'field_news_ticker_text_value',
    'table' => 'node_data_field_news_ticker_text',
    'field' => 'field_news_ticker_text_value',
    'override' => array(
      'button' => 'Override',
    ),
    'relationship' => 'none',
  ),
  'date_filter' => array(
    'operator' => '<=',
    'value' => array(
      'min' => NULL,
      'max' => NULL,
      'value' => NULL,
      'default_date' => 'now',
      'default_to_date' => '',
      ),
      'group' => '0',
      'exposed' => FALSE,
      'expose' => array(
        'operator' => FALSE,
        'label' => '',
      ),
      'date_fields' => array(
        'node_data_field_news_start_date.field_news_start_date_value' => 'node_data_field_news_start_date.field_news_start_date_value',
      ), 
      'date_method' => 'AND',
      'granularity' => 'day',
      'form_type' => 'date_popup',
      'default_date' => 'now',
      'default_to_date' => '',
      'year_range' => '-3:+3',
      'id' => 'date_filter',
      'table' => 'node',
      'field' => 'date_filter',
      'relationship' => 'none',
      'override' => array(
        'button' => 'Override',
      ),
    ),
    'date_filter_1' => array(
      'operator' => '>=',
      'value' => array(
        'min' => NULL,
        'max' => NULL,
        'value' => NULL,
        'default_date' => 'now',
        'default_to_date' => '',
      ),
    'group' => '0',
    'exposed' => FALSE,
    'expose' => array(
      'operator' => FALSE,
      'label' => '',
    ),
    'date_fields' => array(
      'node_data_field_news_end_date.field_news_end_date_value' => 'node_data_field_news_end_date.field_news_end_date_value',
    ),
    'date_method' => 'AND',
    'granularity' => 'day',
    'form_type' => 'date_popup',
    'default_date' => 'now',
    'default_to_date' => '',
    'year_range' => '-3:+3',
    'id' => 'date_filter_1',
    'table' => 'node',
    'field' => 'date_filter',
    'relationship' => 'none',
  ),
)); 

$handler->override_option('access', array( 
  'type' => 'none', 
));
 
$handler->override_option('cache', array( 
  'type' => 'none', 
));
 
$handler->override_option('style_plugin', 'list');
 
$handler->override_option('style_options', array( 
  'grouping' => '', 
  'type' => 'ol', 
));
 
$handler = $view->new_display('page', 'Page', 'page_1');
$handler->override_option('path', 'newsticker');

$handler->override_option('fields', 
  array(
    'title' => array(
    'label' => '',
    'alter' => array(
      'alter_text' => 0,
      'text' => 0,
      'make_link' => 0,
      'path' => '',
      'link_class' => '',
      'alt' => '',
      'prefix' => '',
      'suffix' => '',
      'help' => '',
      'trim' => 0,
      'max_length' => '',
      'word_boundary' => 1,
      'ellipsis' => 1,
      'strip_tags' => 0,
      'html' => 0,
    ),
    'link_to_node' => 1,
    'exclude' => 0,
    'id' => 'title',
    'table' => 'node',
    'field' => 'title',
    'override' => array(
      'button' => 'Override',
    ),
    'relationship' => 'none',
  ),
  'field_news_start_date_value' => array(
    'label' => 'News Start Date',
    'alter' => array(
      'alter_text' => 0,
      'text' => '',
      'make_link' => 0,
      'path' => '',
      'link_class' => '',
      'alt' => '',
      'prefix' => '',
      'suffix' => '',
      'target' => '',
      'help' => '',
      'trim' => 0,
      'max_length' => '',
      'word_boundary' => 1,
      'ellipsis' => 1,
      'html' => 0,
      'strip_tags' => 0,
    ),
    'empty' => '',
    'hide_empty' => 0,
    'empty_zero' => 0,
    'link_to_node' => 0,
    'label_type' => 'widget',
    'format' => 'default',
    'multiple' => array(
      'multiple_number' => '',
      'multiple_from' => '',
      'multiple_to' => '',
      'group' => TRUE,
    ),
    'repeat' => array(
      'show_repeat_rule' => '',
    ),
    'fromto' => array(
      'fromto' => 'both',
    ),
    'exclude' => 0,
    'id' => 'field_news_start_date_value',
    'table' => 'node_data_field_news_start_date',
    'field' => 'field_news_start_date_value',
    'relationship' => 'none',
  ),
  'body' => array(
    'label' => '',
    'alter' => array(
      'alter_text' => 0,
      'text' => '',
      'make_link' => 0,
      'path' => '',
      'link_class' => '',
      'alt' => '',
      'prefix' => '',
      'suffix' => '',
      'help' => '',
      'trim' => 1,
      'max_length' => '400',
      'word_boundary' => 1,
      'ellipsis' => 1,
      'strip_tags' => 1,
      'html' => 1,
    ),
    'exclude' => 0,
    'id' => 'body',
    'table' => 'node_revisions',
    'field' => 'body',
    'relationship' => 'none',
    'override' => array(
      'button' => 'Override',
    ),
  ),
  'view_node' => array(
    'label' => '',
    'alter' => array(
      'alter_text' => 0,
      'text' => '',
      'make_link' => 0,
      'path' => '',
      'link_class' => '',
      'alt' => '',
      'prefix' => '',
      'suffix' => '',
      'help' => '',
      'trim' => 0,
      'max_length' => '',
      'word_boundary' => 1,
      'ellipsis' => 1,
      'strip_tags' => 0,
      'html' => 0,
    ),
    'text' => 'Read more',
    'exclude' => 0,
    'id' => 'view_node',
    'table' => 'node',
    'field' => 'view_node',
    'override' => array(
      'button' => 'Override',
    ),
    'relationship' => 'none',
  ),
));
 
$handler->override_option('menu', array( 
  'type' => 'none', 
  'title' => '', 
  'description' => '', 
  'weight' => 0, 
  'name' => 'navigation', 
));
 
$handler->override_option('tab_options', array( 
  'type' => 'none', 
  'title' => '', 
  'description' => '', 
  'weight' => 0, 
  'name' => 'navigation', 
));
 
$handler = $view->new_display('feed', 'Feed', 'feed_1');
$handler->override_option('style_plugin', 'rss');
 
$handler->override_option('style_options', array( 
  'mission_description' => FALSE, 
  'description' => '', 
));
 
$handler->override_option('row_plugin', 'node_rss');
 
$handler->override_option('row_options', array( 
  'relationship' => 'none', 
  'item_length' => 'teaser', 
));
 
$handler->override_option('path', 'newsticker/rss.xml');
 
$handler->override_option('menu', array( 
  'type' => 'none', 
  'title' => '', 
  'description' => '', 
  'weight' => 0, 
  'name' => 'navigation', 
));
 
$handler->override_option('tab_options', array( 
  'type' => 'none', 
  'title' => '', 
  'description' => '', 
  'weight' => 0, 
  'name' => 'navigation', 
));
 
$handler->override_option('displays', array()); 
$handler->override_option('sitename_title', FALSE); 
$handler = $view->new_display('block', 'Block', 'block_1'); 
$handler->override_option('block_description', ''); 
$handler->override_option('block_caching', -1);

// Register this new view in the main Views array
$views[$view->name] = $view;

return $views;
}