<?php

/*
 * Function called when ticker_views_fields is installed. Adds the below fields to the Page content type
 * 
 * TODO:	Program select box for addition of fields to content type through Drupal admin interface.
 */

function _ticker_views_install_fields() {
  // CCK content_copy.module may not be enabled, so make sure it is included
  require_once './' . drupal_get_path('module', 'content')
      . '/modules/content_copy/content_copy.module';

  $content = _ticker_views_fields_export();
  $form_state = array();
  $form_state['values']['type_name'] = 'page';
  $form_state['value']['macro'] = '$content = ' . var_export($content, TRUE) . ';';

  // content_copy_import_form provided by content_copy.module
  drupal_execute('content_copy_import_form', $form_state);
  content_clear_type_cache();
}

/*
 * Function containing CCK field export array
 */

function _ticker_views_fields_export() {

  $content['groups'] = array(
    0 => array(
      'label' => 'News Ticker',
      'group_type' => 'standard',
      'settings' => array(
        'form' => array(
          'style' => 'fieldset_collapsed',
          'description' => '',
        ),
        'display' => array(
          'description' => '',
          'teaser' => array(
            'format' => 'fieldset',
            'exclude' => 1,
          ),
          'full' => array(
            'format' => 'fieldset',
            'exclude' => 1,
          ),
          4 => array(
            'format' => 'fieldset',
            'exclude' => 1,
          ),
          2 => array(
            'format' => 'fieldset',
            'exclude' => 0,
          ),
          3 => array(
            'format' => 'fieldset',
            'exclude' => 0,
          ),
          5 => array(
            'format' => 'fieldset',
            'exclude' => 1,
          ),
          'label' => 'above',
        ),
      ),
      'weight' => '-2',
      'group_name' => 'group_newsticker',
    ),
  );

  $content['fields'] = array(
    0 => array(
      'label' => 'News Ticker Text',
      'field_name' => 'field_news_ticker_text',
      'type' => 'text',
      'widget_type' => 'text_textfield',
      'change' => 'Change basic information',
      'weight' => '6',
      'rows' => '2',
      'size' => '110',
      'description' => 'Include text here to include a link to this page in the news ticker. The text entered in this field will be displayed as a news item in the news ticker. Max 140 characters. Remove all text from this field to remove the news ticker entry.',
      'default_value' => array(
        0 => array(
          'value' => '',
          '_error_element' => 'default_value_widget][field_news_ticker_text][0][value',
        ),
      ),
      'default_value_php' => '',
      'default_value_widget' => NULL,
      'group' => 'group_newsticker',
      'required' => 0,
      'multiple' => '0',
      'text_processing' => '0',
      'max_length' => '140',
      'allowed_values' => '',
      'allowed_values_php' => '',
      'op' => 'Save field settings',
      'module' => 'text',
      'widget_module' => 'text',
      'columns' => array(
        'value' => array(
          'type' => 'varchar',
          'length' => '140',
          'not null' => false,
          'sortable' => true,
          'views' => true,
        ),
      ),
      'display_settings' => array(
        'weight' => '6',
        'parent' => 'group_newsticker',
        2 => array(
          'format' => 'default',
          'exclude' => 0,
        ),
        3 => array(
          'format' => 'default',
          'exclude' => 0,
        ),
        4 => array(
          'format' => 'default',
          'exclude' => 1,
        ),
        5 => array(
          'format' => 'default',
          'exclude' => 1,
        ),
        'label' => array(
          'format' => 'above',
        ),
        'teaser' => array(
          'format' => 'default',
          'exclude' => 1,
        ),
        'full' => array(
          'format' => 'default',
          'exclude' => 1,
        ),
      ),
    ),
    1 => array(
      'label' => 'News Start Date',
      'field_name' => 'field_news_start_date',
      'type' => 'date',
      'widget_type' => 'date_popup',
      'change' => 'Change basic information',
      'weight' => '7',
      'default_value' => 'now',
      'default_value2' => 'blank',
      'default_value_code' => '',
      'default_value_code2' => '',
      'input_format' => 'F j, Y',
      'input_format_custom' => '',
      'year_range' => '-3:+3',
      'increment' => '1',
      'advanced' => array(
        'label_position' => 'above',
        'text_parts' => array(
          'year' => 0,
          'month' => 0,
          'day' => 0,
          'hour' => 0,
          'minute' => 0,
          'second' => 0,
        ),
      ),
      'label_position' => 'above',
      'text_parts' => array(),
      'description' => 'Date this news ticker item will begin to appear in the site\'s news ticker.',
      'group' => 'group_newsticker',
      'required' => 0,
      'multiple' => '0',
      'repeat' => 0,
      'todate' => '',
      'granularity' => array(
        'year' => 'year',
        'month' => 'month',
        'day' => 'day',
      ),
      'default_format' => 'short',
      'tz_handling' => 'none',
      'timezone_db' => 'America/Chicago',
      'op' => 'Save field settings',
      'module' => 'date',
      'widget_module' => 'date',
      'columns' => array(
        'value' => array(
          'type' => 'varchar',
          'length' => 20,
          'not null' => false,
          'sortable' => true,
          'views' => true,
        ),
      ),
      'display_settings' => array(
        'weight' => '7',
        'parent' => 'group_newsticker',
        2 => array(
          'format' => 'default',
          'exclude' => 0,
        ),
        3 => array(
          'format' => 'default',
          'exclude' => 0,
        ),
        4 => array(
          'format' => 'default',
          'exclude' => 1,
        ),
        5 => array(
          'format' => 'default',
          'exclude' => 1,
        ),
        'label' => array(
          'format' => 'above',
        ),
        'teaser' => array(
          'format' => 'default',
          'exclude' => 1,
        ),
        'full' => array(
          'format' => 'default',
          'exclude' => 1,
        ),
      ),
    ),
    2 => array(
      'label' => 'News End Date',
      'field_name' => 'field_news_end_date',
      'type' => 'date',
      'widget_type' => 'date_popup',
      'change' => 'Change basic information',
      'weight' => '8',
      'default_value' => 'blank',
      'default_value2' => 'same',
      'default_value_code' => '',
      'default_value_code2' => '',
      'input_format' => 'F j, Y',
      'input_format_custom' => '',
      'year_range' => '-3:+3',
      'increment' => '1',
      'advanced' => array(
        'label_position' => 'above',
        'text_parts' => array(
          'year' => 0,
          'month' => 0,
          'day' => 0,
          'hour' => 0,
          'minute' => 0,
          'second' => 0,
        ),
      ),
      'label_position' => 'above',
      'text_parts' => array(),
      'description' => 'Date this news ticker item will automatically stop appearing in the site\'s news ticker.',
      'group' => 'group_newsticker',
      'required' => 0,
      'multiple' => '0',
      'repeat' => 0,
      'todate' => '',
      'granularity' => array(
        'year' => 'year',
        'month' => 'month',
        'day' => 'day',
      ),
      'default_format' => 'short',
      'tz_handling' => 'none',
      'timezone_db' => 'America/Chicago',
      'op' => 'Save field settings',
      'module' => 'date',
      'widget_module' => 'date',
      'columns' => array(
        'value' => array(
          'type' => 'varchar',
          'length' => 20,
          'not null' => false,
          'sortable' => true,
          'views' => true,
        ),
      ),
      'display_settings' => array(
        'weight' => '8',
        'parent' => 'group_newsticker',
        2 => array(
          'format' => 'default',
          'exclude' => 0,
        ),
        3 => array(
          'format' => 'default',
          'exclude' => 0,
        ),
        4 => array(
          'format' => 'default',
          'exclude' => 1,
        ),
        5 => array(
          'format' => 'default',
          'exclude' => 1,
        ),
        'label' => array(
          'format' => 'above',
        ),
        'teaser' => array(
          'format' => 'default',
          'exclude' => 1,
        ),
        'full' => array(
          'format' => 'default',
          'exclude' => 1,
        ),
      ),
    ),
    3 => array(
      'label' => 'News Sort Priority',
      'field_name' => 'field_news_sort_priority',
      'type' => 'number_integer',
      'widget_type' => 'optionwidgets_select',
      'change' => 'Change basic information',
      'weight' => '9',
      'description' => 'Value to determine news items sorting order. The higher the value, the higher the item will appear in the news list. (10=highest, 1=lowest, 5=default). News items with the same priority will be sorted according to publish date, with more recent entries higher and older entries lower.',
      'default_value' => array(
        0 => array(
          'value' => '5',
        ),
      ),
      'default_value_php' => '',
      'default_value_widget' => array(
        'field_news_sort_priority' => array(
          'value' => '5',
        ),
      ),
      'group' => 'group_newsticker',
      'required' => 1,
      'multiple' => '0',
      'min' => '',
      'max' => '',
      'prefix' => '',
      'suffix' => '',
      'allowed_values' => '10 9 8 7 6 5 4 3 2 1 ',
      'allowed_values_php' => '',
      'op' => 'Save field settings',
      'module' => 'number',
      'widget_module' => 'optionwidgets',
      'columns' => array(
        'value' => array(
          'type' => 'int',
          'not null' => false,
          'sortable' => true,
        ),
      ),
      'display_settings' => array(
        'weight' => '9',
        'parent' => 'group_newsticker',
        2 => array(
          'format' => 'default',
          'exclude' => 1,
        ),
        3 => array(
          'format' => 'default',
          'exclude' => 1,
        ),
        4 => array(
          'format' => 'default',
          'exclude' => 1,
        ),
        5 => array(
          'format' => 'default',
          'exclude' => 1,
        ),
        'label' => array(
          'format' => 'above',
        ),
        'teaser' => array(
          'format' => 'default',
          'exclude' => 1,
        ),
        'full' => array(
          'format' => 'default',
          'exclude' => 1,
        ),
      ),
    ),
  );
  $content['extra'] = array(
    'title' => '-5',
    'body_field' => '-1',
    'revision_information' => '1',
    'author' => '0',
    'options' => '2',
    'comment_settings' => '4',
    'menu' => '-3',
    'taxonomy' => '-4',
    'path' => '5',
    'print' => '3',
  );

  return $content;
}
